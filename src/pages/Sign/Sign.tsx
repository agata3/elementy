import { Info } from "../../components/Info";
import { LinkHome } from "../../components/LinkHome";
import ScrollUp from "../../components/ScrollUp";
import { Form } from "./Form";
export const Sign = () => {
  return (
    <div>
      <LinkHome />
      <Form />
      <Info />
      <ScrollUp />
    </div>
  );
};
