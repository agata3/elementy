import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(0, 3),
    marginBottom: theme.spacing(3),
  },
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },

  form: {
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 3),
    width: 150,
    borderColor: "#333333",
  },
}));

export const Form = () => {
  const classes = useStyles();
  return (
    <div className={classes.paper}>
      <div className={classes.container}>
        <div style={{ width: "100%", marginTop: 100 }}>
          <Typography component="div" style={{ color: "#333333" }}>
            <Box fontSize={18} textAlign="center" fontWeight="fontWeightMedium">
              LOG IN
            </Box>
          </Typography>
        </div>
        <form className={classes.form} noValidate>
          <Grid container spacing={2} style={{ marginBottom: 30 }}>
            <Grid item xs={12} sm={6}>
              <TextField
                size="small"
                autoComplete="fname"
                name="Name"
                // variant="outlined"
                required
                fullWidth
                id="Name"
                label=" Name"
                autoFocus
                color="secondary"
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                size="small"
                inputProps={{
                  style: {},
                }}
                color="secondary"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
              />
            </Grid>
          </Grid>
          <Box textAlign="center">
            <Button
              type="submit"
              variant="outlined"
              style={{ borderRadius: 0 }}
              className={classes.submit}
            >
              SIGN IN
            </Button>
          </Box>
          <Typography component="div" color="secondary">
            <Box
              marginBottom={2}
              fontSize={17}
              textAlign="center"
              fontWeight="fontWeightRegular"
              style={{ textDecorationLine: "underline" }}
            >
              Forgot your password?
            </Box>
            <Typography component="div" style={{ color: "#333333" }}>
              <Box
                marginTop={2}
                fontSize={18}
                textAlign="center"
                fontWeight="fontWeightMedium"
              >
                SIGN UP
              </Box>
            </Typography>
          </Typography>
          <Box textAlign="center" marginTop={4}>
            <Button
              type="submit"
              variant="outlined"
              style={{ borderRadius: 0 }}
              className={classes.submit}
            >
              REGISTER
            </Button>
          </Box>
        </form>
      </div>
    </div>
  );
};
