import { Box, Typography } from "@material-ui/core";
import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",

      paddingTop: theme.spacing(5),
    },
  })
);
export const Latest = () => {
  const classes = useStyles();
  return (
    <div>
      <Typography component="div" className={classes.root}>
        <Box fontSize={24} marginLeft={-18}>
          LATEST ARRIVALS
        </Box>
      </Typography>
    </div>
  );
};
