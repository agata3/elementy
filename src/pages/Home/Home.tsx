import { usePictureData } from "../../utils/usePictureData";
import { TextPicture } from "../../components/TextPicture";
import { MainTextPicture } from "../../components/MainTextPicture";
import { ImageList } from "../../components/imageList";
import { homeGLData } from "../../utils/homeGLData";
import { Info } from "../../components/Info";
import { Latest } from "./Latest";
import ScrollUp from "../../components/ScrollUp";
export const Home = () => {
  return (
    <div>
      <MainTextPicture
        img={usePictureData().mainHomePicture.img}
        text1={usePictureData().mainHomePicture.text1}
        text2={usePictureData().mainHomePicture.text2}
        text3={usePictureData().mainHomePicture.text3}
        height={usePictureData().mainHomePicture.height}
      />
      <TextPicture
        img={usePictureData().homePicture1.img}
        text1={usePictureData().homePicture1.text1}
        height={usePictureData().homePicture1.height}
      />
      <TextPicture
        img={usePictureData().homePicture2.img}
        text1={usePictureData().homePicture2.text1}
        height={usePictureData().homePicture2.height}
      />
      <Latest />
      <ImageList list={homeGLData} />
      <TextPicture
        img={usePictureData().homePicture3.img}
        text1={usePictureData().homePicture3.text1}
        height={usePictureData().homePicture3.height}
      />
      <Info />
      <ScrollUp />
    </div>
  );
};
