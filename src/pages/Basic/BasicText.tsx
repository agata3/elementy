import { Box, Typography } from "@material-ui/core";
import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",

      paddingTop: theme.spacing(2),
    },
  })
);
export const BasicText = () => {
  const classes = useStyles();
  return (
    <div>
      <Typography component="div" className={classes.root}>
        <Box fontSize={15} paddingLeft={2} paddingRight={1}>
          <strong>3 FOR 2 -BUY MIN.</strong> three products from Basic
          Collection and pay for the price of two.
        </Box>
        <Box fontSize={15}>Use the code 3ZA2 to receive the discount.</Box>
      </Typography>
    </div>
  );
};
