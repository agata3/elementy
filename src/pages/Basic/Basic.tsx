import { usePictureData } from "../../utils/usePictureData";
import { TextPicture } from "../../components/TextPicture";
import { ImageList } from "../../components/imageList";
import Box from "@material-ui/core/Box";
import { basicGLData } from "../../utils/basicGLData";
import { Info } from "../../components/Info";
import { LinkHome } from "../../components/LinkHome";
import ScrollUp from "../../components/ScrollUp";
import { BasicText } from "./BasicText";

export const Basic = () => {
  return (
    <div style={{ backgroundColor: "#fafafa" }}>
      <LinkHome />
      <Box style={{ width: "100%" }}>
        <BasicText />
        <Box padding={2}>
          <TextPicture
            img={usePictureData().pictureBasic1.img}
            height={usePictureData().pictureBasic1.height}
          />
        </Box>
        <ImageList list={basicGLData.slice(0, 6)} />
        <Box padding={2}>
          <TextPicture
            img={usePictureData().pictureBasic2.img}
            height={usePictureData().pictureBasic2.height}
          />
        </Box>
        <ImageList list={basicGLData.slice(6)} />
        <Info />
        <ScrollUp />
      </Box>
    </div>
  );
};
