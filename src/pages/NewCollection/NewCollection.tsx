import { usePictureData } from "../../utils/usePictureData";
import { TextPicture } from "../../components/TextPicture";
import { ImageList } from "../../components/imageList";
import Box from "@material-ui/core/Box";
import { newCollectionGLData } from "../../utils/newCollectionGLData";
import { Info } from "../../components/Info";
import { LinkHome } from "../../components/LinkHome";
import ScrollUp from "../../components/ScrollUp";
import { Latest } from "../Home/Latest";

export const NewCollection = () => {
  return (
    <div style={{ backgroundColor: "#fafafa" }}>
      <LinkHome />

      <Box style={{ width: "100%" }}>
        <Latest />
        <Box padding={2}>
          <TextPicture
            img={usePictureData().newPictureCollection3.img}
            height={usePictureData().newPictureCollection3.height}
            text3={usePictureData().newPictureCollection3.text3}
          />
        </Box>
        <ImageList list={newCollectionGLData.slice(0, 3)} />

        <Box padding={2}>
          <TextPicture
            img={usePictureData().newPictureCollection2.img}
            height={usePictureData().newPictureCollection2.height}
          />
        </Box>
        <ImageList list={newCollectionGLData.slice(3, 7)} />
        <TextPicture
          img={usePictureData().newPictureCollection1.img}
          height={usePictureData().newPictureCollection1.height}
        />
        <ImageList list={newCollectionGLData.slice(7)} />
        <TextPicture
          img={usePictureData().newPictureCollection4.img}
          height={usePictureData().newPictureCollection4.height}
        />
        <Info />

        <ScrollUp />
      </Box>
    </div>
  );
};
