import { usePictureData } from "../../utils/usePictureData";
import { TextPicture } from "../../components/TextPicture";
import { ImageList } from "../../components/imageList";
import Box from "@material-ui/core/Box";
import { accessoriesGLData } from "../../utils/accessoriesGLData";
import { Info } from "../../components/Info";
import { LinkHome } from "../../components/LinkHome";
import ScrollUp from "../../components/ScrollUp";

export const Accessories = () => {
  return (
    <div style={{ backgroundColor: "#fafafa" }}>
      <LinkHome />
      <Box style={{ width: "100%" }}>
        <Box padding={2}>
          <TextPicture
            img={usePictureData().pictureAccessories1.img}
            height={usePictureData().pictureAccessories1.height}
          />
        </Box>
        <ImageList list={accessoriesGLData.slice(0, 5)} />
        <Box padding={2}>
          <TextPicture
            img={usePictureData().pictureAccessories2.img}
            height={usePictureData().pictureAccessories2.height}
          />
        </Box>
        <ImageList list={accessoriesGLData.slice(5, 10)} />
        <Box padding={2}>
          <TextPicture
            img={usePictureData().pictureAccessories3.img}
            height={usePictureData().pictureAccessories3.height}
          />
        </Box>
        <ImageList list={accessoriesGLData.slice(10)} />
        <Info />
        <ScrollUp />
      </Box>
    </div>
  );
};
