import { LinkHome } from "../../components/LinkHome";
import { FaqMainText } from "./FaqMainText";
import { List } from "../../components/List";
import { FaqList } from "./FaqList";
import { faqData } from "../../utils/faqData";
import { Info } from "../../components/Info";
import ScrollUp from "../../components/ScrollUp";
export const Faq = () => {
  return (
    <div>
      <LinkHome />
      <FaqMainText />
      <List Component={FaqList} list={faqData} />
      <Info />
      <ScrollUp />
    </div>
  );
};
