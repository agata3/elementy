import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import { Box, Typography } from "@material-ui/core";
import { faqDataProps } from "../../utils/faqData";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",

      backgroundColor: "#fafafa;",
    },
    root1: {
      paddingBottom: 40,

      backgroundColor: "#fafafa;",
    },
    inline: {
      display: "inline",
    },
  })
);
export interface Faq {
  item: faqDataProps;
}

export const FaqList: React.FC<Faq> = ({ item }) => {
  const classes = useStyles();
  return (
    <div key={item.key} className={classes.root1}>
      <Typography component="div">
        <Box
          letterSpacing={2}
          paddingTop={5}
          fontSize={23}
          display="flex"
          justifyContent="center"
        >
          {item.mainText}
        </Box>
      </Typography>

      <List className={classes.root}>
        <ListItem alignItems="flex-start">
          <ListItemIcon style={{ paddingTop: 5 }}>
            <FiberManualRecordIcon style={{ fontSize: 10 }} />
          </ListItemIcon>
          <ListItemText style={{ marginLeft: -40 }}>
            <Typography style={{ fontSize: 18 }}>{item.text1}</Typography>
          </ListItemText>
        </ListItem>
        <ListItemText
          secondary={
            <Typography
              style={{ marginLeft: 32, fontSize: 17, marginRight: 15 }}
            >
              {item.text2}
            </Typography>
          }
        />
      </List>

      <List className={classes.root}>
        <ListItem alignItems="flex-start">
          <ListItemIcon style={{ paddingTop: 5 }}>
            <FiberManualRecordIcon style={{ fontSize: 10 }} />
          </ListItemIcon>
          <ListItemText style={{ marginLeft: -40 }}>
            <Typography style={{ fontSize: 18 }}>{item.text11}</Typography>
          </ListItemText>
        </ListItem>
        <ListItemText
          secondary={
            <Typography
              style={{ marginLeft: 32, fontSize: 17, marginRight: 15 }}
            >
              {item.text3}
            </Typography>
          }
        />
      </List>
    </div>
  );
};
