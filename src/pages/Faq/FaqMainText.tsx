import { Box, Typography } from "@material-ui/core";
export const FaqMainText = () => {
  return (
    <div style={{ width: "100%" }}>
      <Typography component="div">
        <Box marginTop={5} fontSize={22} display="flex" justifyContent="center">
          Frequently asked
        </Box>
        <Box fontSize={22} display="flex" justifyContent="center">
          questions
        </Box>
      </Typography>
    </div>
  );
};
