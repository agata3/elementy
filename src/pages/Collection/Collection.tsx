import { usePictureData } from "../../utils/usePictureData";
import { TextPicture } from "../../components/TextPicture";
import { ImageList } from "../../components/imageList";
import Box from "@material-ui/core/Box";
import { collectionGLData } from "../../utils/collectionGLData";
import { Info } from "../../components/Info";
import { LinkHome } from "../../components/LinkHome";
import ScrollUp from "../../components/ScrollUp";
export const Collection = () => {
  return (
    <>
      <LinkHome />
      <div style={{ backgroundColor: "#fafafa" }}>
        <Box padding={2}>
          <TextPicture
            img={usePictureData().pictureCollection1.img}
            height={usePictureData().pictureCollection1.height}
            backgroundPositionY={
              usePictureData().pictureCollection1.backgroundPositionY
            }
          />
        </Box>
        <Box padding={2}>
          <TextPicture
            img={usePictureData().pictureCollection2.img}
            height={usePictureData().pictureCollection2.height}
          />
        </Box>
        <ImageList list={collectionGLData.slice(0, 5)} />
        <Box paddingLeft={2} paddingRight={2} paddingTop={3} paddingBottom={2}>
          <TextPicture
            img={usePictureData().pictureCollection3.img}
            height={usePictureData().pictureCollection3.height}
          />
        </Box>
        <Box paddingLeft={2} paddingRight={2} paddingTop={3}>
          <TextPicture
            img={usePictureData().pictureCollection4.img}
            height={usePictureData().pictureCollection4.height}
          />
        </Box>
        <ImageList list={collectionGLData.slice(5, 7)} />
        <Box paddingLeft={2} paddingRight={2} paddingTop={3}>
          <TextPicture
            img={usePictureData().pictureCollection5.img}
            height={usePictureData().pictureCollection5.height}
          />
        </Box>
        <ImageList list={collectionGLData.slice(7)} />
        <Box paddingLeft={2} paddingRight={2} paddingTop={3}>
          <TextPicture
            img={usePictureData().pictureCollection6.img}
            height={usePictureData().pictureCollection6.height}
          />
        </Box>
        <Box paddingLeft={2} paddingRight={2} paddingTop={3}>
          <TextPicture
            img={usePictureData().pictureCollection7.img}
            height={usePictureData().pictureCollection7.height}
          />
        </Box>
        <Info />
        <ScrollUp />
      </div>
    </>
  );
};
