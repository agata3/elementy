import { createMuiTheme } from "@material-ui/core/styles";

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#ff9800",
    },
    secondary: {
      main: "#3d3c3c",
    },
  },
  typography: {
    fontFamily: "Hind Siliguri",
  },
  overrides: {
    MuiFormControlLabel: {
      label: {
        fontSize: "0.775rem",
      },
    },
    MuiToolbar: {
      gutters: {
        paddingLeft: 5,
        paddingRight: 5,
      },
    },
    MuiContainer: {
      root: {
        paddingLeft: 0,
        paddingRight: 0,
      },
    },
  },
});
