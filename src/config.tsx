import { Home } from "./pages/Home";
import { NewCollection } from "./pages/NewCollection";
import { Sign } from "./pages/Sign";
import { Faq } from "./pages/Faq";
import { Basic } from "./pages/Basic/Basic";
import { Collection } from "./pages/Collection/Collection";
import { Accessories } from "./pages/Accessories";

interface PageConfig {
  component: React.ReactElement;
  title: string;
  url: string;
}

const pagesConfig: Omit<PageConfig, "url">[] = [
  { title: "HOME", component: <Home /> },
  { title: "NEW COLLECTION", component: <NewCollection /> },
  { title: "COLLECTION", component: <Collection /> },
  { title: "BASIC", component: <Basic /> },
  { title: "ACCESSORIES", component: <Accessories /> },
];
export const pages: PageConfig[] = pagesConfig.map((page) => ({
  ...page,
  url: `/${page.title === "Home" ? "" : page.title.toLowerCase()}`,
}));

interface PageOptionConfig {
  component: React.ReactElement;
  title: string;
  url: string;
}

const pagesOptionConfig: Omit<PageOptionConfig, "url">[] = [
  { title: "SIGN IN", component: <Sign /> },
  { title: "FAQ", component: <Faq /> },
  // { title: "TERM AND CONDITIONS OF USE", component: <Term /> },
];
export const pagesOption: PageOptionConfig[] = pagesOptionConfig.map(
  (page) => ({
    ...page,
    url: `/${page.title === "Home" ? "" : page.title.toLowerCase()}`,
  })
);
