import { MuiThemeProvider } from "@material-ui/core/styles";
import { theme } from "../MuiTheme";
import { Route } from "react-router-dom";
import { pages } from "../config";
import { pagesOption } from "../config";
import { FullDialog } from "../components/FullDialog";
import Container from "@material-ui/core/Container";
function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <Container maxWidth="lg">
        <FullDialog />
        {pages.map((page) => (
          <Route exact key={page.title} path={page.url}>
            {page.component}
          </Route>
        ))}
        {/* export const pages: PageConfig[] = pagesConfig.map((page) => ({
  ...page,
  url: `/${page.title === "Home" ? "" : page.title.toUpperCase()}`,
})); */}
        {pagesOption.map((page) => (
          <Route exact key={page.title} path={page.url}>
            {page.component}
          </Route>
        ))}
      </Container>
    </MuiThemeProvider>
  );
}

export default App;
