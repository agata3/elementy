import { Box, Grid, Typography, Button } from "@material-ui/core";

export interface MainTextPictureProps {
  img: string;
  height: number;
  text1?: string;
  text2?: string;
  text3?: string;
  backgroundPositionY?: number;
  backgroundPositionX?: number;
}

export const MainTextPicture: React.FC<MainTextPictureProps> = ({
  img,
  height,
  text1,
  text2,
  text3,
  backgroundPositionY,
  backgroundPositionX,
}) => {
  return (
    <Box display="flex" position="relative">
      <Grid container>
        <Grid
          item
          xs={12}
          style={{
            backgroundImage: `url(${img})`,
            backgroundSize: "cover",
            height,

            backgroundPositionY,
            backgroundPositionX,
          }}
        ></Grid>
      </Grid>

      <Box position="absolute" alignSelf="center" style={{ width: "100%" }}>
        <Typography component="div">
          <Box display="flex" justifyContent="center">
            <Box>
              <Box
                fontSize={50}
                fontWeight={800}
                paddingLeft={5}
                style={{
                  color: "#FFF",
                }}
              >
                {text1}
              </Box>
              <Button>
                <Box
                  fontSize={18}
                  fontWeight={500}
                  style={{
                    color: "#FFF",
                    paddingLeft: 20,
                    paddingRight: 20,
                  }}
                >
                  {text2}
                </Box>
              </Button>
            </Box>
          </Box>
        </Typography>
      </Box>
      <Box position="absolute" alignSelf="flex-end" style={{ width: "100%" }}>
        <Typography component="div">
          <Box display="flex" justifyContent="center">
            <Box
              p={5}
              fontSize={15}
              style={{
                color: "#FFF",
              }}
            >
              {text3}
            </Box>
          </Box>
        </Typography>
      </Box>
    </Box>
  );
};
