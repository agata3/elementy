import Link from "@material-ui/core/Link";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import Box from "@material-ui/core/Box";

export const LinkHome = () => {
  return (
    <div style={{ width: "100%" }}>
      <Box alignItems="center" display="flex" p={2}>
        <ArrowBackIosIcon style={{ fontSize: "18" }} color="secondary" />
        <Link href="../Home" style={{ color: "black" }}>
          <Box fontSize={18}>Home</Box>
        </Link>
      </Box>
    </div>
  );
};
