import { Box, Grid, Typography } from "@material-ui/core";

export interface TextPictureProps {
  img: string;
  height: number;
  text1?: string;
  text2?: string;
  text3?: string;
  backgroundPositionY?: number;
  backgroundPositionX?: number;
}

export const TextPicture: React.FC<TextPictureProps> = ({
  img,
  height,
  text1,
  text2,
  text3,
  backgroundPositionY,
  backgroundPositionX,
}) => {
  return (
    <Box display="flex" position="relative">
      <Grid container>
        <Grid
          item
          xs={12}
          style={{
            backgroundImage: `url(${img})`,
            backgroundSize: "cover",
            height,

            backgroundPositionY,
            backgroundPositionX,
          }}
        ></Grid>
      </Grid>

      <Box position="absolute" alignSelf="flex-start">
        <Typography component="div">
          <Box
            p={2}
            fontSize={23}
            textAlign="center"
            fontWeight={400}
            style={{
              color: "#FFF",
            }}
          >
            {text1}
          </Box>
        </Typography>
      </Box>

      <Box position="absolute" alignSelf="center" style={{ width: "100%" }}>
        <Typography component="div">
          <Box display="flex" justifyContent="center">
            <Box
              p={5}
              fontSize={30}
              fontWeight={500}
              style={{
                color: "#FFF",
              }}
            >
              {text2}
            </Box>
          </Box>
        </Typography>
      </Box>
      <Box position="absolute" alignSelf="flex-end">
        <Typography component="div">
          <Box
            p={2}
            fontSize={20}
            // fontWeight={500}
            style={{
              color: "#FFF",
            }}
          >
            {text3}
          </Box>
        </Typography>
      </Box>
    </Box>
  );
};
