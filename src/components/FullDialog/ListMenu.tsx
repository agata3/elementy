import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { pages } from "../../config";
import { Link, Box } from "@material-ui/core";
import { Link as RouterLink } from "react-router-dom";
import { Search } from "./Search";
import IconButton from "@material-ui/core/IconButton";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import { ListMenuOptions } from "./ListMenuOptions";
import { ListMenuFooter } from "./ListMenuFooter";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      maxWidth: 440,
      backgroundColor: theme.palette.background.paper,
    },
  })
);
const text = {
  fontSize: "1.2em",
  fontWeight: 500,
};

export interface ListMenuProps {
  handleClose?: any;
}

export const ListMenu: React.FC<ListMenuProps> = ({ handleClose }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Search />
      <List component="nav" aria-label="main mailbox folders">
        {pages
          //   .filter((page) => page.title !== "Home")
          .map((page) => {
            return (
              <Box key={page.title}>
                <Link
                  key={page.title}
                  component={RouterLink}
                  onClick={handleClose}
                  to={page.url}
                  // to={`/${
                  //   page.title === "Home" ? "" : page.title.toLowerCase()
                  // }`}

                  color="inherit"
                >
                  <Box style={{ width: "100%" }}>
                    <Box paddingX={1}>
                      <Box>
                        <ListItem
                          button
                          // onClick={() => {
                          //   handleClose();
                          //   push(page.url);
                          // }}
                        >
                          <ListItemText
                            primaryTypographyProps={{ style: text }}
                            primary={page.title}
                            style={{ color: "black", fontWeight: "bold" }}
                          />
                          <IconButton edge="end">
                            <NavigateNextIcon style={{ color: "black" }} />
                          </IconButton>
                        </ListItem>
                      </Box>
                    </Box>
                  </Box>
                </Link>
              </Box>
            );
          })}
      </List>
      <ListMenuOptions handleClose={handleClose} />
      <ListMenuFooter />
    </Box>
  );
};
