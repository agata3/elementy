import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import { Box } from "@material-ui/core";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import { TransitionProps } from "@material-ui/core/transitions";
import { ListMenu } from "./ListMenu";
import { AppBar } from "./AppBar";
import ShoppingBasketOutlinedIcon from "@material-ui/icons/ShoppingBasketOutlined";
import Link from "@material-ui/core/Link";
// import { MainTextPicture } from "../MainTextPicture";
// import { usePictureData } from "../../utils/usePictureData";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      position: "relative",
      backgroundColor: theme.palette.background.paper,
      borderBottom: "#e0e0e0 solid 1px",
      paddingTop: 2,
    },

    root: {
      backgroundColor: theme.palette.background.paper,
    },
  })
);

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export const FullDialog = () => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Box className={classes.root}>
      <AppBar handleClickOpen={handleClickOpen} />
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <Toolbar>
          <Box style={{ width: "100%" }} className={classes.appBar}>
            <Box
              display="flex"
              alignItems="center"
              justifyContent="space-between"
            >
              <Box>
                <Box>
                  <IconButton
                    color="secondary"
                    onClick={handleClose}
                    aria-label="close"
                  >
                    <CloseIcon style={{ fontSize: 32 }} />
                  </IconButton>
                </Box>
              </Box>
              <Box>
                <Typography component="div">
                  <Box
                    style={{ color: "black" }}
                    fontSize={23}
                    letterSpacing={10}
                    fontWeight="fontWeightLight"
                  >
                    <Link
                      href="/Home"
                      style={{ color: "black", textDecorationLine: "none" }}
                    >
                      ELEMENTY
                    </Link>
                  </Box>
                </Typography>
              </Box>
              <Box>
                <IconButton color="secondary">
                  <ShoppingBasketOutlinedIcon style={{ fontSize: 22 }} />
                </IconButton>
              </Box>
            </Box>
          </Box>
        </Toolbar>
        <ListMenu handleClose={handleClose} />
      </Dialog>
      <Box>
        {/* <MainTextPicture
          height={usePictureData().mainHomePicture1.height}
          img={usePictureData().mainHomePicture1.img}
        /> */}
      </Box>
    </Box>
  );
};
