import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import InstagramIcon from "@material-ui/icons/Instagram";
import Typography from "@material-ui/core/Typography";
import PinterestIcon from "@material-ui/icons/Pinterest";
import TwitterIcon from "@material-ui/icons/Twitter";
import { Box, IconButton, Button } from "@material-ui/core";
import FacebookIcon from "@material-ui/icons/Facebook";
import YouTubeIcon from "@material-ui/icons/YouTube";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    appBar: {
      paddingX: 1,
      flexGrow: 1,
      position: "fixed",
      bottom: theme.spacing(0),
      right: theme.spacing(0),
      backgroundColor: "transparent",
      boxShadow: "none",
      borderTop: "#e0e0e0 solid 1px",
    },
    menuButton: {
      margin: theme.spacing(1),
    },
    title: {
      flexGrow: 1,
    },
  })
);

export const ListMenuFooter = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Box display="flex">
          <Typography className={classes.title}>
            <Button>
              <Box
                fontWeight="fontWeightRegular"
                fontSize={19}
                flexGrow={1}
                className={classes.menuButton}
                color="inherit"
                aria-label="menu"
              >
                PL / EN
              </Box>
            </Button>
          </Typography>

          <IconButton color="secondary">
            <FacebookIcon style={{ fontSize: 25, marginRight: 8 }} />
            <InstagramIcon style={{ fontSize: 25, marginRight: 8 }} />
            <YouTubeIcon style={{ fontSize: 25, marginRight: 8 }} />
            <PinterestIcon style={{ fontSize: 25, marginRight: 8 }} />
            <TwitterIcon style={{ fontSize: 25 }} />
          </IconButton>
        </Box>
      </AppBar>
    </div>
  );
};
