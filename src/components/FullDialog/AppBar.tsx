import MenuIcon from "@material-ui/icons/Menu";
import { Box, Typography, IconButton } from "@material-ui/core";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import ShoppingBasketOutlinedIcon from "@material-ui/icons/ShoppingBasketOutlined";
import Link from "@material-ui/core/Link";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      backgroundColor: theme.palette.background.paper,
      // zIndex: 1,
      // paddingTop: 4,
      // marginTop: -8,
    },

    root: {
      backgroundColor: theme.palette.background.paper,
    },
  })
);

export interface ListMenuProps {
  handleClickOpen?: any;
}
export const AppBar: React.FC<ListMenuProps> = ({ handleClickOpen }) => {
  const classes = useStyles();
  return (
    <div style={{ width: "100%" }} className={classes.appBar}>
      <Box display="flex" alignItems="center" justifyContent="space-between">
        <Box>
          <Box>
            <IconButton
              color="secondary"
              onClick={handleClickOpen}
              aria-label="close"
            >
              <MenuIcon style={{ fontSize: 32 }} color="secondary" />
            </IconButton>
          </Box>
        </Box>
        <Box>
          <Typography component="div">
            <Box
              style={{ color: "black" }}
              fontSize={23}
              letterSpacing={10}
              fontWeight="fontWeightLight"
            >
              <Link
                href="/Home"
                style={{ color: "black", textDecorationLine: "none" }}
              >
                ELEMENTY
              </Link>
            </Box>
          </Typography>
        </Box>
        <Box>
          <IconButton color="secondary">
            <ShoppingBasketOutlinedIcon style={{ fontSize: 22 }} />
          </IconButton>
        </Box>
      </Box>
    </div>
  );
};
