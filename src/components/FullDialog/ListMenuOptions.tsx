import { pagesOption } from "../../config";
import { Link, Box } from "@material-ui/core";
import { Link as RouterLink } from "react-router-dom";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

export interface ListMenuProps {
  handleClose?: any;
}
const text = {
  fontSize: "1.2em",
  fontWeight: 500,
};

export const ListMenuOptions: React.FC<ListMenuProps> = ({ handleClose }) => {
  return (
    <Box style={{ width: "100%" }} mt={5}>
      <List component="nav" aria-label="main mailbox folders">
        {pagesOption.map((page) => {
          return (
            <Box key={page.title}>
              <Link
                style={{ textDecoration: "none" }}
                key={page.title}
                component={RouterLink}
                onClick={handleClose}
                to={page.url}
                color="inherit"
              >
                <div style={{ width: "100%" }}>
                  <Box paddingX={1}>
                    <Box>
                      <ListItem button>
                        <ListItemText
                          primary={page.title}
                          primaryTypographyProps={{ style: text }}
                        />
                      </ListItem>
                    </Box>
                  </Box>
                </div>
              </Link>
            </Box>
          );
        })}
      </List>
    </Box>
  );
};
