import React from "react";

export interface ListProps {
  Component: React.FC<{ item: any }>;
  list: { key: number | string }[];
}

export const List: React.FC<ListProps> = ({ Component, list }) => {
  return (
    <>
      {list.map((item) => (
        <Component item={item} key={item.key} />
      ))}
    </>
  );
};
