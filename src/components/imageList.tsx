import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import { Box, Typography } from "@material-ui/core";
import { homeData } from "../utils/homeGLData";
import Grid from "@material-ui/core/Grid";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      backgroundColor: "#fafafa",
      marginTop: theme.spacing(5),
    },
    gridList: {
      marginBottom: theme.spacing(2),
      height: 360,
    },
    container: {
      marginTop: theme.spacing(4),
    },
  })
);
export interface List {
  list: homeData[];
}
export const ImageList: React.FC<List> = ({ list }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={2} className={classes.container}>
        {list.map((tile) => (
          <Grid item xs={6} className={classes.gridList} key={tile.key}>
            <Box
              style={{
                backgroundImage: `url(${tile.img})`,
                backgroundSize: "cover",
                height: 265,
              }}
            />
            <Typography component="div">
              <Box marginTop={3} fontSize={15} ml={1}>
                {tile.text}
              </Box>
              <Box fontSize={15} ml={7}>
                {tile.price}
              </Box>
            </Typography>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};
