import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

export const Contact = () => {
  return (
    <div
      style={{
        width: "100%",
        backgroundColor: "#f8f5f3",
        paddingBottom: 20,
        paddingLeft: 18,
      }}
    >
      <Typography component="div" color="secondary">
        <Box display="flex" p={1} flexDirection="column">
          <Box fontSize={15} style={{ color: "#8a8a8a" }} marginY={1}>
            CUSTOMER CARE
          </Box>
          <Box fontSize={14}>shop@elememtywear.com</Box>
          <Box fontSize={14}>+48 535 185 189</Box>
          <Box fontSize={13}>mon - fri. 9:00 - 5:00</Box>
        </Box>
      </Typography>
    </div>
  );
};
