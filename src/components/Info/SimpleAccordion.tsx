import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import { Box, Typography, Grid, Link } from "@material-ui/core";

import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
    },
    heading: {
      fontSize: theme.typography.pxToRem(14),
    },
    textSecondary: {
      fontSize: theme.typography.pxToRem(14),
    },
  })
);

export const SimpleAccordion = () => {
  const classes = useStyles();

  return (
    <div className={classes.root} style={{ backgroundColor: "#f8f5f3" }}>
      <Accordion
        style={{ backgroundColor: "#f8f5f3", borderTop: "solid #d1cfcf 1px" }}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon style={{ fontSize: 20 }} />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Box style={{ width: "100%" }}>
            <Box display="flex" alignItems="center">
              <Typography component="div">
                <Box className={classes.heading}>SELECTED SHOPS</Box>
              </Typography>
            </Box>
          </Box>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className={classes.textSecondary} component="div">
            <Box style={{ width: "100%" }}>
              <Typography component="div" color="secondary">
                <Box display="flex" p={1} flexDirection="column">
                  <Box fontSize={15} style={{ color: "#8a8a8a" }} marginY={1}>
                    VISIT US
                  </Box>
                  <Box fontSize={14} marginTop={3}>
                    WARSZAWA
                  </Box>
                  <Box fontSize={14}>Department Store MYSIA 3,</Box>
                  <Box fontSize={14}>Mysia 3</Box>
                  <Box fontSize={13}>+48 535 204 796</Box>

                  <Box fontSize={14} marginTop={3}>
                    Elektrownia Powisie,
                  </Box>
                  <Box fontSize={13}>Dobra 42</Box>
                  <Box fontSize={13}>+48 908 204 296</Box>

                  <Box fontSize={14} marginTop={3}>
                    KATOWICE
                  </Box>
                  <Box fontSize={14}>Department Store MYSIA 3,</Box>
                  <Box fontSize={14}>Mysia 3</Box>
                  <Box fontSize={13}>+48 535 204 796</Box>
                  <Box fontSize={14} marginTop={3}>
                    WROCLAW
                  </Box>
                  <Box fontSize={14}>Pan Pablo,</Box>
                  <Box fontSize={14}>52 Pilsudskiego Street</Box>
                  <Box fontSize={13}>+48 535 204 796</Box>
                </Box>
              </Typography>
            </Box>
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ backgroundColor: "#f8f5f3" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon style={{ fontSize: 20 }} />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Box style={{ width: "100%" }}>
            <Box display="flex" alignItems="center">
              <Typography component="div">
                <Box className={classes.heading}>HELP</Box>
              </Typography>
            </Box>{" "}
          </Box>
        </AccordionSummary>{" "}
        <AccordionDetails>
          <Typography className={classes.textSecondary} component="div">
            <Box style={{ width: "100%" }}>
              <Typography component="div" color="secondary">
                <Box display="flex" p={1} flexDirection="column">
                  <Box fontSize={14}>Delivery</Box>
                  <Box fontSize={14}>Pre-order</Box>
                  <Box fontSize={13}>Exchange and returns</Box>
                  <Box fontSize={13}>Gift cards </Box>
                  <Box fontSize={13}>Terms and conditions</Box>
                  <Box fontSize={13}>3 for 2 promotion</Box>
                  <Box fontSize={13}>Relove</Box>
                  <Box fontSize={13}>FAQ</Box>
                  <Box fontSize={13}>Privacy</Box>
                  <Box fontSize={13}>Contact</Box>
                </Box>
              </Typography>
            </Box>
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ backgroundColor: "#f8f5f3" }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon style={{ fontSize: 20 }} />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Box style={{ width: "100%" }}>
            <Box display="flex" alignItems="center">
              <Typography component="div">
                <Box className={classes.heading} fontWeight="fontWeightMedium">
                  TRANSPARENT SHOPPING COLLECTIVE
                </Box>
              </Typography>
            </Box>
          </Box>
        </AccordionSummary>
        <AccordionDetails>
          <Box className={classes.textSecondary}>
            <Box style={{ width: "100%" }}>
              <Typography component="div" color="secondary">
                <Box display="flex" p={1} flexDirection="column">
                  <Box
                    fontSize={15}
                    style={{ color: "#8a8a8a" }}
                    marginTop={1}
                    marginBottom={2}
                    marginRight={16}
                  >
                    PART OF TRANSPARENT SHOPPING COLLECTIVE
                  </Box>
                  <Box fontSize={13}>
                    We decided to take a step and be pioners.
                  </Box>
                  <Box fontSize={13}>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Fugiat, dolores.
                  </Box>
                  <Box fontSize={12}>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Debitis architecto eius, reiciendis cupiditate autem
                    consequuntur eligendi vel quam maxime accusantium!
                  </Box>
                  <Box marginTop={2}>
                    <Grid container style={{ marginBottom: 10 }}>
                      <Grid
                        item
                        xs={3}
                        style={{
                          backgroundImage: `url(${"https://elementywear.com/themes/elementy-theme/assets/img/footer/swof.png"})`,
                          backgroundSize: "cover",
                          height: 80,
                        }}
                      ></Grid>
                    </Grid>
                    <Grid container>
                      <Grid
                        item
                        xs={4}
                        style={{
                          backgroundImage: `url(${"https://elementywear.com/themes/elementy-theme/assets/img/footer/ts.jpg"})`,
                          backgroundSize: "contain",
                          height: 98,
                        }}
                      ></Grid>
                    </Grid>
                    <Box
                      fontSize={14}
                      fontWeight="fontWeightMedium"
                      marginTop={2}
                    >
                      <Link>
                        Learn more about our price amet consectetur adipisicing.
                      </Link>
                    </Box>
                  </Box>
                </Box>
              </Typography>
            </Box>
          </Box>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};
