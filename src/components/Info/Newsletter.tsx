import React from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(0, 3),
    backgroundColor: "#f8f5f3",
  },
  paper: {
    paddingTop: theme.spacing(6),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },

  form: {
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export const Newsletter = () => {
  const classes = useStyles();

  return (
    <Container component="main" maxWidth="xs" className={classes.container}>
      <CssBaseline />
      <div className={classes.paper}>
        <div style={{ width: "100%" }}>
          <Typography component="div" color="secondary">
            <Box display="flex" style={{ paddingLeft: 3.5 }}>
              <Box fontSize={15} style={{ color: "#8a8a8a" }}>
                NEWSLETTER - GET 10% OFF
              </Box>
            </Box>
          </Typography>
        </div>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                size="small"
                autoComplete="fname"
                name="Name"
                // variant="outlined"
                required
                fullWidth
                id="Name"
                label=" Name"
                autoFocus
                color="secondary"
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                size="small"
                inputProps={{
                  style: {},
                }}
                color="secondary"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
              />
            </Grid>

            <Grid item xs={12}>
              <FormControlLabel
                control={
                  <Checkbox
                    value="allowExtraEmails"
                    style={{ color: "#696767" }}
                  />
                }
                label="I have reviewed and understand the Privacy Policy*"
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            style={{ backgroundColor: "black", color: "white" }}
            className={classes.submit}
          >
            JOIN NEWSLETTER
          </Button>
        </form>
      </div>
    </Container>
  );
};
