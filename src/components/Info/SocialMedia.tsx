import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import YouTubeIcon from "@material-ui/icons/YouTube";
import PinterestIcon from "@material-ui/icons/Pinterest";
import TwitterIcon from "@material-ui/icons/Twitter";
export const SocialMedia = () => {
  return (
    <div
      style={{
        width: "100%",
        paddingTop: 25,
        paddingBottom: 30,
        paddingLeft: 25,
        backgroundColor: "#f8f5f3",
      }}
    >
      <Typography component="div" color="secondary">
        <Box display="flex">
          <Box fontSize={14} style={{ color: "#8a8a8a" }} marginBottom={2}>
            SOCIAL MEDIA
          </Box>
        </Box>
      </Typography>
      <Box>
        <Box>
          <FacebookIcon style={{ margin: 5 }} />
          <InstagramIcon style={{ margin: 5 }} />
          <PinterestIcon style={{ margin: 5 }} />
          <YouTubeIcon style={{ margin: 5 }} />
          <TwitterIcon style={{ margin: 5 }} />
        </Box>
      </Box>
      <Typography component="div" color="secondary">
        <Box display="flex" marginTop={7} marginBottom={1}>
          <Box fontSize={12} style={{ color: "#8a8a8a" }}>
            All rights reserved - ELEMENTY
          </Box>
        </Box>
      </Typography>
    </div>
  );
};
