import { Contact } from "./Contact";
import { Newsletter } from "./Newsletter";
import { SimpleAccordion } from "./SimpleAccordion";
import { SocialMedia } from "./SocialMedia";

export const Info = () => {
  return (
    <div>
      <Contact />
      <SimpleAccordion />
      <Newsletter />
      <SocialMedia />
    </div>
  );
};
