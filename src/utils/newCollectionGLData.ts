export interface newCollectionData {
  key: number;
  text: string;
  img: string;
  price: string;
}

export const newCollectionGLData: newCollectionData[] = [
  {
    key: 11,
    text: "WANDA TRENCH COAT",
    img: "https://elementywear.com/modules/pxtoolbox/uploads/629f6324265355.04186531.jpeg",
    price: "€241.42",
  },
  {
    key: 3,
    text: "RINN TRENCH COAT",
    img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
    price: "€250",
  },
  {
    key: 9,
    text: "OVERSIZE JACKET",
    img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
    price: "€299.50",
  },
  {
    key: 10,
    text: "OVERSIZE JACKET BLACK",
    img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
    price: "€239.50",
  },

  {
    key: 4,
    text: "SPENCER JACKET BLUE",
    img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
    price: "€160",
  },
  {
    key: 5,
    text: "SPENCER JACKET BEIGE",
    img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
    price: "€160",
  },
  {
    key: 6,
    text: "OVERSIZE JACKET BLUE",
    img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
    price: "€150",
  },
  {
    key: 14,
    text: "ANNE SKIRT BLACK",
    img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
    price: "€112.78",
  },
  //Next
  {
    key: 1,
    text: "PLATTER DRESS",
    img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
    price: "€110.50",
  },
  {
    key: 7,
    text: "MINI REGULAR JEANS NATURAL",
    img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
    price: "€99.50",
  },
  {
    key: 13,
    text: "RINGO TROUSERS",
    img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
    price: "€89.22",
  },
  {
    key: 8,
    text: "AMAR SWEATER WHITE",
    img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
    price: "€179.50",
  },
  {
    key: 2,
    text: "ODME SWEATER WHITE",
    img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
    price: "€90.98",
  },
  //NEXT

  {
    key: 12,
    text: "LOTTA BLOUSE WHITE",
    img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
    price: "€99.22",
  },

  {
    key: 15,
    text: "SILK SCARF YELLOW",
    img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
    price: "€110.78",
  },
];
