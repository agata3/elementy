export interface accessoriesData {
  key: number;
  text: string;
  img: string;
  price: string;
}

export const accessoriesGLData: accessoriesData[] = [
  {
    key: 10,
    text: "SCRUNCHIES ZERO WASTE BLACK",
    img: "https://elementywear.com/5265-medium_default/scrunchies.jpg",
    price: "€10.12",
  },
  {
    key: 11,
    text: "SCRUNCHIES ZERO WASTE RED",
    img: "https://elementywear.com/5262-medium_default/scrunchies.jpg",
    price: "€10.12",
  },
  {
    key: 12,
    text: "SCRUNCHIES ZERO WASTE DARK GREEN",
    img: "https://elementywear.com/5263-medium_default/scrunchies.jpg",
    price: "€10.12",
  },
  {
    key: 13,
    text: "RELOVE TOTLE BACK BLUE-RED ",
    img: "https://elementywear.com/5277-medium_default/relove-tote-bag.jpg",
    price: "€11.12",
  },
  {
    key: 14,
    text: "RELOVE TOTLE BACK GREEN-BLUE-RED ",
    img: "https://elementywear.com/5276-medium_default/relove-tote-bag.jpg",
    price: "€11.12",
  },
  {
    key: 5,
    text: "COLOR SOCKS TAUPE",
    img: " https://elementywear.com/5680-medium_default/block-socks.jpg",
    price: "€11.12",
  },
  {
    key: 6,
    text: "COLOR SOCKS RED",
    img: " https://elementywear.com/6443-medium_default/block-socks.jpg",
    price: "€11.12",
  },
  {
    key: 7,
    text: "COLOR SOCKS YELLOW",
    img: " https://elementywear.com/6436-medium_default/block-socks.jpg",
    price: "€11.12",
  },
  {
    key: 8,
    text: "COLOR SOCKS PINK",
    img: " https://elementywear.com/5684-medium_default/block-socks.jpg",
    price: "€11.12",
  },
  {
    key: 9,
    text: "COLOR SOCKS MINT",
    img: " https://elementywear.com/5681-medium_default/block-socks.jpg",
    price: "€11.12",
  },

  {
    key: 1,
    text: "SILK SCARF BLACK",
    img: " https://elementywear.com/6459-medium_default/silk-foulard.jpg",
    price: "€89.48",
  },
  {
    key: 2,
    text: "SILK SCARF ORANGE",
    img: " https://elementywear.com/6461-medium_default/silk-foulard.jpg",
    price: "€89.48",
  },
  {
    key: 3,
    text: "SILK SCARF YELLOW",
    img: " https://elementywear.com/6453-medium_default/silk-foulard.jpg",
    price: "€89.48",
  },
  {
    key: 4,
    text: "SILK SCARF PINK",
    img: " https://elementywear.com/6455-medium_default/silk-foulard.jpg",
    price: "€89.48",
  },
];
