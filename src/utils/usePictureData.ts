export const usePictureData = () => {
  const homeData = {
    mainHomePicture: {
      img: "https://elementywear.com/modules/pxtoolbox/uploads/629f6324265355.04186531.jpeg",
      text1: "I AM MAJA",
      text2: "DISCOVER OUR NEW COLLECTION  >",
      text3: "SCROLL FOR MORE",
      height: 500,
    },
    mainHomePicture1: {
      img: "https://elementywear.com/modules/pxtoolbox/uploads/629f6324265355.04186531.jpeg",
      height: 800,
    },
    homePicture1: {
      img: "https://elementywear.com/10880-prod/cesaria-new-dress.jpg",
      text1: "NEW IN",
      height: 500,
    },
    homePicture2: {
      img: "https://elementywear.com/10947-medium_default/noel-shirt.jpg",
      text1: "COATS",
      height: 500,
    },
    homePicture3: {
      img: "https://elementywear.com/11189-prod/cesaria-short-dress.jpg",
      text1: "COLLECTION",
      height: 500,
    },
    // pictureCollection
    pictureCollection1: {
      img: "https://elementywear.com/modules/categoriesbanners/views/img/1,2.jpg",
      height: 240,
      backgroundPositionY: 30,
    },
    pictureCollection2: {
      img: "https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
      height: 240,
    },
    pictureCollection3: {
      img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
      height: 500,
    },
    pictureCollection4: {
      img: "https://elementywear.com/modules/categoriesbanners/views/img/7.jpg",
      height: 500,
    },
    pictureCollection5: {
      img: "https://elementywear.com/modules/categoriesbanners/views/img/12.jpg",
      height: 470,
    },
    pictureCollection6: {
      img: "https://elementywear.com/modules/categoriesbanners/views/img/20.jpg",
      height: 500,
    },
    pictureCollection7: {
      img: "https://elementywear.com/modules/categoriesbanners/views/img/18,19.jpg",
      height: 300,
    },
    //newpictureCollection
    newPictureCollection1: {
      img: "	https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
      height: 500,
    },
    newPictureCollection2: {
      img: "https://elementywear.com/modules/categoriesbanners/views/img/1280x890_7-8_ENG.jpg",
      height: 250,
    },

    newPictureCollection3: {
      img: "https://elementywear.com/modules/categoriesbanners/views/img/590x900_1.jpg",
      height: 500,
      text3: "Selena coat",
    },
    newPictureCollection4: {
      img: "https://elementywear.com/10414-prod/mimi-jeans-long.jpg",
      height: 600,
    },
    //basic
    pictureBasic1: {
      img: "	https://elementywear.com/10848-medium_default/guccio.jpg",
      height: 200,
    },
    pictureBasic2: {
      img: "https://elementywear.com/modules/categoriesbanners/views/img/6_eng.jpg",
      height: 500,
    },
    //accessories
    pictureAccessories1: {
      img: "https://elementywear.com/modules/categoriesbanners/views/img/590x900_51_ENG.jpg",
      height: 500,
    },
    pictureAccessories2: {
      img: "https://elementywear.com/modules/categoriesbanners/views/img/acc10.jpg",
      height: 500,
    },
    pictureAccessories3: {
      img: "https://elementywear.com/modules/categoriesbanners/views/img/acc1718.jpg",
      height: 300,
    },
  };
  return homeData;
};
