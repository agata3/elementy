export interface homeData {
  key: number;
  text: string;
  img: string;
  price: string;
}

export const homeGLData: homeData[] = [
  {
    key: 1,
    text: "WANDA TRENCH COAT",
    img: "https://elementywear.com/modules/pxtoolbox/uploads/629f6324265355.04186531.jpeg",
    price: "€241.42",
  },
  {
    key: 2,
    text: "SELENA TRENCH COAT",
    img: "	https://elementywear.com/10848-medium_default/guccio.jpg",
    price: "€199.22",
  },
  {
    key: 3,
    text: "PLATTER DRESS",
    img: "	https://elementywear.com/10848-medium_default/guccio.jpg",
    price: "€112.78",
  },
  {
    key: 4,
    text: "SELENA TRENCH COAT",
    img: "	https://elementywear.com/10848-medium_default/guccio.jpg",
    price: "€299.52",
  },
];
