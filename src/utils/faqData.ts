export interface faqDataProps {
  key: number;
  mainText: string;
  text1: string;
  text2: string;
  text3: string;
  text11: string;
}
export const faqData: faqDataProps[] = [
  {
    key: 1,
    mainText: "ORDERS AND PAYMENTS",
    text1: "What payment method do you accept?",
    text11: "Can i cancel my order?",
    text2:
      "At Elementy Online Store we accept payments via Tpay services for orders with shipment in Poland. For foreign orders: via bank transfer, online card payment (Stripe), PayPal, Giropay, Ideal, Bancontact, Sofort, EPS, Multibanco. For PayPal transactions a non-refundable 3% of the order value commission is charged.",
    text3:
      "We are depending  the product, we periodically supplement the warehouse. If you are interested in a product that is not currently in stock, we encourage you to sign up for availability notifications, you can do it on the product page, choosing the size you are interested in.",
  },
  {
    key: 2,
    mainText: "ONLINE STORE",
    text1: "How do i make purchases?",
    text11: "How can i contact Elementy?",
    text2:
      "Yes, if your order hasn’t been paid yet.  You already completed the transaction, but your parcel hasn’t been shipped yet, let us know at shop@elementywear.com so that we can cancel your order.",
    text3:
      "Depending on the product, we periodically supplement the warehouse.online card payment (Stripe), PayPal, Giropay, Ideal, Bancontact, Sofort, EPS, Multibanco. For PayPal transactions a non-refundable 3% of the order value in a product that is not currently in stock, we encourage you to sign up for availability notifications, you can do it on the product page, choosing the size you are interested in.",
  },
  {
    key: 3,
    mainText: "BOUTIQUES",
    text1: "Does Elementy have any boutiques",
    text11: "Can i cancel my order?",
    text2:
      "Unfortunately, it is not possible to change the ordered products (model, color, size, quantity) address given in the order, please write to shop@elementywear.com as soon as possible. It is not possible to change the address after generating the waybill.",
    text3:
      "Depending on the product, supplement the warehouse. Are interested in a product that is not currently in stock, we encourage you to sign up for availability notifications, you can do it on the product page, choosing the size you are interested in.",
  },
  {
    key: 4,
    mainText: "TAILORING",
    text1: "Do you offer tailoring service?",
    text11: "Can i cancel my order?",
    text2:
      "We periodically supplement the warehouse. If you are interested  currently in stock, we encourage you to sign up for availability notifications, you can do it on the product page, choosing the size you are interested in.",
    text3:
      "Depending on the product, we periodically supplement the warehouse  you are interested in a product that is not currently in stock, we encourage you to sign up for availability notifications, you can do it on the product page, choosing the size you are interested in.",
  },
  {
    key: 5,
    mainText: "COSTUMER COMPLAITS",
    text1: "Can i feale a complinet",
    text11: "Can i cancel my order?",
    text2:
      " We periodically supplement the warehouse. Product that is not currently , we encourage you to sign up for availability notifications, you can do it on the product page.",
    text3:
      "Supplement the  you are interested in a product that is not currently in stock, we encourage you to sign up for availability notifications, you can do it on the product page, choosing the size you are interested in.",
  },
];
