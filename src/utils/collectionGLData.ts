export interface collectionData {
  key: number;
  text: string;
  img: string;
  price: string;
}

export const collectionGLData: collectionData[] = [
  {
    key: 1,
    text: "GAMMA BLOUSE BLACK",
    img: "https://elementywear.com/11057-medium_default/raider-top.jpg",
    price: "€72.60",
  },
  {
    key: 2,
    text: "GAMMA BLOSE CAMEL",
    img: "https://elementywear.com/11057-medium_default/raider-top.jpg",
    price: "€72.60",
  },
  {
    key: 3,
    text: "GAMMA BLOSE ROUGE",
    img: "https://elementywear.com/11057-medium_default/raider-top.jpg",
    price: "€72.60",
  },
  {
    key: 4,
    text: "GAMMA BLOSE GRAY",
    img: "https://elementywear.com/11057-medium_default/raider-top.jpg",
    price: "€72.60",
  },
  //Next
  {
    key: 5,
    text: "MINI REGULAR JEANS NAVY",
    img: "https://elementywear.com/9747-medium_default/jimmy-new-top-.jpg",
    price: "€99.50",
  },
  {
    key: 6,
    text: "MINI REGULAR JEANS BLUE",
    img: "https://elementywear.com/9747-medium_default/jimmy-new-top-.jpg",
    price: "€99.50",
  },
  {
    key: 7,
    text: "MINI REGULAR JEANS NATURAL",
    img: "https://elementywear.com/9747-medium_default/jimmy-new-top-.jpg",
    price: "€99.50",
  },
  {
    key: 8,
    text: "AMAR SWEATER WHITE",
    img: "https://elementywear.com/9747-medium_default/jimmy-new-top-.jpg",
    price: "€179.50",
  },
  //NEXT
  {
    key: 9,
    text: "OVERSIZE JACKET",
    img: "https://elementywear.com/9747-medium_default/jimmy-new-top-.jpg",
    price: "€299.50",
  },
  {
    key: 10,
    text: "OVERSIZE JACKET BLACK",
    img: "https://elementywear.com/9747-medium_default/jimmy-new-top-.jpg",
    price: "€239.50",
  },
  //NEXT
  {
    key: 11,
    text: "WANDA TRENCH COAT",
    img: "https://elementywear.com/9747-medium_default/jimmy-new-top-.jpg",
    price: "€241.42",
  },
  {
    key: 12,
    text: "SELENA TRENCH COAT",
    img: "https://elementywear.com/9747-medium_default/jimmy-new-top-.jpg",
    price: "€199.22",
  },
  {
    key: 13,
    text: "PLATTER DRESS",
    img: "https://elementywear.com/9747-medium_default/jimmy-new-top-.jpg",
    price: "€112.78",
  },
];
