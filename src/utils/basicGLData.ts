export interface basicData {
  key: number;
  text: string;
  img: string;
  price: string;
}

export const basicGLData: basicData[] = [
  {
    key: 1,
    text: "RENE T-SHIRT BLACK",
    img: "https://elementywear.com/9732-medium_default/bas-t-shirt.jpg",
    price: "€39.78",
  },
  {
    key: 2,
    text: "RENE T-SHIRT WHITE",
    img: "https://elementywear.com/9732-medium_default/bas-t-shirt.jpg",
    price: "€39.78",
  },
  {
    key: 3,
    text: "KEMEN T-SHIRT BLACK",
    img: "https://elementywear.com/9732-medium_default/bas-t-shirt.jpg",
    price: "€36.78",
  },
  {
    key: 4,
    text: "KEMEN T-SHIRT WHITE",
    img: "https://elementywear.com/9732-medium_default/bas-t-shirt.jpg",
    price: "€36.78",
  },
  {
    key: 5,
    text: "PEDRO T-SHIRT BLACK",
    img: "https://elementywear.com/9732-medium_default/bas-t-shirt.jpg",
    price: "€39.78",
  },
  {
    key: 6,
    text: "DYLAN T-SHIRT WHITE",
    img: "https://elementywear.com/9732-medium_default/bas-t-shirt.jpg",
    price: "€39.78",
  },
  {
    key: 7,
    text: "CARLOS TOP BLACK",
    img: "https://elementywear.com/4456-medium_default/carlos-top.jpg",
    price: "€49.78",
  },
  {
    key: 8,
    text: "CARLOS TOP GRAY",
    img: "https://elementywear.com/4453-medium_default/carlos-top.jpg",
    price: "€45.78",
  },
  {
    key: 9,
    text: "JAMES T-SHIRT BLACK",
    img: "https://elementywear.com/9732-medium_default/bas-t-shirt.jpg",
    price: "€35.78",
  },
  {
    key: 10,
    text: "JOHN BODY RUSSET",
    img: "	https://elementywear.com/9742-medium_default/bas-t-shirt.jpg",
    price: "€49.78",
  },
  {
    key: 11,
    text: "JOHN BODY PINK",
    img: "	https://elementywear.com/9742-medium_default/bas-t-shirt.jpg",
    price: "€49.78",
  },
  {
    key: 12,
    text: "JOHN BODY OFF",
    img: "	https://elementywear.com/9742-medium_default/bas-t-shirt.jpg",
    price: "€49.78",
  },
];
